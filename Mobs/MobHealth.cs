using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobHealth : MonoBehaviour,IClickable
{
   public Action<MobHealth> MobDied;
   
   private int _hp;
   private Animator _animator;

   private void Start()
   {
      _animator = GetComponent<Animator>();
      _hp = GameManager.Instance.GetExtraHp;
   }

   public void Click()
   {
      GetDamage(1);
   }

   public void GetDamage(int damage)
   {
      _hp-=damage;
      
      if (_hp <= 0)
      {
         Die();
      }
      else
      {
         _animator.Play("GetHit");
      }
      
   }

   public void Die()
   {
      _animator.Play("Die2");
   }

   private void RemoveMob()
   {
      MobDied?.Invoke(this);
      DataManager.Instance.RemoveMob();
      Destroy(gameObject);
   }
}
