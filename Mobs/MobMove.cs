using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MobMove : MonoBehaviour
{
   
    private int _speed;

    private Vector3 _targetPoint;

    private Rigidbody _rigidbody;
    
    private Vector3 _startPoint;

    private Vector3 _endPoint;

    private void Start()
    {
        _targetPoint=GetTargetPoint();
        _rigidbody = GetComponent<Rigidbody>();
        _speed = GameManager.Instance.GetExtraSpeed;

    }

    private void FixedUpdate()
    {
        MoveToPoint();
    }

    private void MoveToPoint()
    {
        if (Vector3.Distance(transform.position, _targetPoint) > 3)
        {
            var dir = (_targetPoint - transform.position).normalized;
            _rigidbody.AddForce(dir * _speed);
            _rigidbody.velocity = Vector3.zero;
        }
        else
        {
            _targetPoint = GetTargetPoint();
        }
        var rotation = Quaternion.LookRotation(_targetPoint);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime*10);

    }

    private Vector3 GetTargetPoint()
    {
        var position1 = GameManager.Instance.GetLeftTopBorder();
        var position2 = GameManager.Instance.GetRightBottomBorder();
        
        float randomX = Random.Range(position1.x, position2.x);
        float randomZ = Random.Range(position1.z, position2.z);
        
        var vector = new Vector3(randomX, 0,randomZ);
        return vector;
        
    }
}
