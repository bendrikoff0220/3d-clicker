using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DataManager : MonoBehaviour
{
   [SerializeField] private TextMeshProUGUI _scoreText;
   
   [SerializeField] private TextMeshProUGUI _mobsCountText;
   public static DataManager Instance=>_instance;
   private static DataManager _instance;

   private int _score;

   private int _mobsCount;
   private void Awake()
   {
      _instance = this;
   }
   private void Start()
   {
      _scoreText.text = _score.ToString();
   }
   private void AddScore()
   {
      _score++;
      _scoreText.text = _score.ToString();
   }
   public void AddMob()
   {
      _mobsCount++;
      _mobsCountText.text = _mobsCount.ToString();
      if (_mobsCount >= 10)
      {
         GameManager.Instance.GameEnded?.Invoke();
      }
   }
   public void RemoveMob()
   {
      _mobsCount--;
      _mobsCountText.text = _mobsCount.ToString();
      AddScore();
   }
}
