using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> _mobsPrefs;
    [SerializeField] private List<GameObject> _bonusesPrefs;
    
    public static Spawner Instance=>_instance;
    private static Spawner _instance;

    private List<MobHealth> _mobHealths;
    
    private bool _isSpawning;
    
    private void Awake()
    {
        _instance = this;
        GameManager.Instance.GameStarted += SpawnerStart;
        GameManager.Instance.GameEnded += SpawnerStop;

    }

    private void Start()
    {
        _mobHealths = new List<MobHealth>();
    }

    private IEnumerator MobSpawn()
    {
        if (!_isSpawning) yield break;
        
        var position1 = GameManager.Instance.GetLeftTopBorder();
        var position2 = GameManager.Instance.GetRightBottomBorder();
        
        float randomX = Random.Range(position1.x, position2.x);
        float randomZ = Random.Range(position1.z, position2.z);

        var position = new Vector3(randomX, 0, randomZ);
        
        var mob = Instantiate(_mobsPrefs[Random.Range(0, _mobsPrefs.Count)], position, new Quaternion());
        var mobHealth = mob.GetComponent<MobHealth>();
        _mobHealths.Add(mobHealth);
        mobHealth.MobDied += RemoveMob;
        
        
        DataManager.Instance.AddMob();
        
        yield return new WaitForSeconds(1f/GameManager.Instance.GetExtraSpawnSpeed);
        StartCoroutine(MobSpawn());

    }

    private IEnumerator BonusSpawn()
    {
        if (!_isSpawning) yield break;
        
        var position1 = GameManager.Instance.GetLeftTopBorder();
        var position2 = GameManager.Instance.GetRightBottomBorder();
        
        float randomX = Random.Range(position1.x, position2.x);
        float randomZ = Random.Range(position1.z, position2.z);

        var position = new Vector3(randomX, 0, randomZ);
        
        var mob = Instantiate(_bonusesPrefs[Random.Range(0, _bonusesPrefs.Count)], position, new Quaternion());
        yield return new WaitForSeconds(GameManager.Instance.GetBonusSpawnSpeed);
        StartCoroutine(BonusSpawn());
    }

    public void SpawnerStart()
    {
        _isSpawning = true;
        StartCoroutine(MobSpawn());
        StartCoroutine(BonusSpawn());
    }

    public void SpawnerStop()
    {
        _isSpawning = false;
    }

    public void KillAll()
    {
        foreach (var health in _mobHealths)
        {
            health.Die();
        }
    }

    private void RemoveMob(MobHealth health)
    {
        _mobHealths.Remove(health);
        health.MobDied-=RemoveMob;
    }


}
