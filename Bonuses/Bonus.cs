using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bonus : MonoBehaviour,IClickable
{
    private void Start()
    {
        StartCoroutine(LifeTimer());
    }

    public abstract void Click();

    private IEnumerator LifeTimer()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
}
