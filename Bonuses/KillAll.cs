using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAll : Bonus
{
   public override void Click()
   {
      Spawner.Instance.KillAll();
      Destroy(gameObject);
   }
}
