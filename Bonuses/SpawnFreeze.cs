using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

public class SpawnFreeze : Bonus
{
    public override async void Click()
    {
        StopTimer();
        Destroy(gameObject);

    }

    private async void StopTimer()
    {
        Spawner.Instance.SpawnerStop();
        await Task.Delay(3000);
        Debug.Log("Ку ку");
        Spawner.Instance.SpawnerStart();
    }
}
