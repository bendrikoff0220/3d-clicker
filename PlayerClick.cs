using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClick : MonoBehaviour
{


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
            if (Physics.Raycast(ray, out hit))
            {
                var clickable = hit.transform.gameObject.GetComponent<IClickable>();
                if (clickable != null)
                {
                    clickable.Click();
                }

            }
        }
    }
}
