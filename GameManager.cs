using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _startWindow;

    [SerializeField] private GameObject _endWindow;
    public static GameManager Instance=>_instance;
    private static GameManager _instance;

    public Action GameStarted;

    public Action GameEnded;
    
    public int GetExtraSpeed=>_extraSpeed;
    private int _extraSpeed;

    public int GetExtraHp=>_extraHp;
    private int _extraHp;
    
    public float GetExtraSpawnSpeed=>_extraSpawnSpeed;
    private float _extraSpawnSpeed;

    public float GetBonusSpawnSpeed => _bonusSpawnSpeed;
    private float _bonusSpawnSpeed;
    
    private Camera _camera;

    private void Awake()
    {
        _instance = this;
        _camera=Camera.main;
    }
    
    public void StartGame()
    {
        _bonusSpawnSpeed = 3f;
        StartCoroutine(AddExtraMultiples());
        GameStarted?.Invoke();
        GameEnded += EndGame;
        _startWindow.SetActive(false);
    }
    
    public void EndGame()
    {
        _endWindow.SetActive(true);
    }

    public Vector3 GetLeftTopBorder()
    {
        var vector = _camera.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f));
        return new Vector3(vector.x, 0, vector.z);
    }
    public Vector3 GetRightBottomBorder()
    {
        var vector = _camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0f));
        return new Vector3(vector.x, 0, vector.z);
    }

    public IEnumerator AddExtraMultiples()
    {
        _extraSpeed += 20;
        _extraHp += 1;
        _extraSpawnSpeed += 0.5f;

        yield return new WaitForSeconds(10);
        StartCoroutine(AddExtraMultiples());
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene("SampleScene");
    }
    
    
    
    
    
}
